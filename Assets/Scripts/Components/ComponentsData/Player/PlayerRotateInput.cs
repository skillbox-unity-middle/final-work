﻿using Unity.Entities;
using Unity.Mathematics;

namespace Components.ComponentsData.Player
{
    public struct PlayerRotateInput : IComponentData
    {
        public float2 Value;
        public float2 Delta;
    }
}