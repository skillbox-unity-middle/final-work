using Unity.Entities;

namespace Components.ComponentsData.Player
{
    public struct DamageMultiplierComponent : IComponentData
    {
        public float Value;
    }
}