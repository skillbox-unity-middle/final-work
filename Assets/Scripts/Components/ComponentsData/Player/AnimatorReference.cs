using Unity.Entities;
using UnityEngine;

namespace Components.ComponentsData.Player
{
    public class AnimatorReference : IComponentData
    {
        public Animator Value;
    }
}