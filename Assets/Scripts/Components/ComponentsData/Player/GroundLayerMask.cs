using Unity.Entities;
using UnityEngine;

namespace Components.ComponentsData.Player
{
    public struct GroundLayerMask : IComponentData
    {
        public LayerMask Value;
    }
}