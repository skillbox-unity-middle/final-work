using Unity.Entities;

namespace Components.ComponentsData.Player
{
    public struct DefenseComponent : IComponentData
    {
        public float Value;
    }
}