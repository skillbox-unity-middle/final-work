﻿using Unity.Entities;
using Unity.Mathematics;

namespace Components.ComponentsData.Player
{
    public struct PlayerMoveInput : IComponentData
    {
        public float2 Value;
    }
}