using Unity.Entities;

namespace Components.ComponentsData.Buffs
{
    public struct BuffSpawner : IComponentData
    {
        public Entity RateOfFireBuffEntityPrefab;
        public Entity MoveSpeedBuffEntityPrefab;
        public Entity HealthPointBuffEntityPrefab;
        public Entity DefenseBuffEntityPrefab;
        public Entity DamageBuffEntityPrefab;
        public float ChanceToDrop;
    }
}