using Unity.Entities;

namespace Components.ComponentsData.Buffs
{
    public struct BuffComponent : IComponentData
    {
        public float Value;
        public StatTypes StatToModify;
    }
    
    public enum StatTypes
    {
        Damage,
        Defense,
        HealthPoint,
        RateOfFire,
        MoveSpeed,
        None
    }
}