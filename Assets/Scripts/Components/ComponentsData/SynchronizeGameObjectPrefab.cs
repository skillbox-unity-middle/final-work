﻿using Unity.Entities;
using UnityEngine;

namespace Components.ComponentsData
{
    public class SynchronizeGameObjectPrefab : IComponentData
    {
        public GameObject Value;
    }
}