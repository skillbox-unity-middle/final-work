using Unity.Entities;

namespace Components.ComponentsData.Damage
{
    public struct DamageDealer : IComponentData
    {
        public int Value;
    }
}