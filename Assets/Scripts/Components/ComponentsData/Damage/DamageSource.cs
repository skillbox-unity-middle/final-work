using Unity.Entities;

namespace Components.ComponentsData.Damage
{
    public struct DamageSource : IComponentData
    {
        public bool IsPlayer;
    }
}