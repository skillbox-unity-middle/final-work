using Unity.Entities;

namespace Components.ComponentsData.Damage
{
    [InternalBufferCapacity(10)]
    public struct DamageTaker : IBufferElementData, IEnableableComponent
    {
        public int Value;
    }
}