﻿using Unity.Entities;
using UnityEngine;

namespace Components.ComponentsData
{
    public class SynchronizeGameObject : ICleanupComponentData
    {
        public GameObject Value;
    }
}

