using Unity.Entities;

namespace Components.ComponentsData.Spawners
{
    public struct EnemiesCount : IComponentData
    {
        public int Value;
    }
}