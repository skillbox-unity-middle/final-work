﻿using Unity.Entities;

namespace Components.ComponentsData.Spawners
{
    public struct SpawnerEntityPrefab : IComponentData
    {
        public Entity Value;
    }
}