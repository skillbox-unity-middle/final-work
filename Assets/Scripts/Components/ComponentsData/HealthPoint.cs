using Unity.Entities;

namespace Components.ComponentsData
{
    public struct HealthPoint : IComponentData
    {
        public int MaxHp;
        public int CurrentHp;
    }
}