using Unity.Entities;
using Unity.Mathematics;

namespace Components.ComponentsData.Enemies
{
    public struct RandomGenerator : IComponentData
    {
        public Random Value;
    }
}