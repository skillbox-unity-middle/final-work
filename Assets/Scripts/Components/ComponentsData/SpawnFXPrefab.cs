using Unity.Entities;
using UnityEngine;

namespace Components.ComponentsData
{
    public class SpawnFXPrefab : IComponentData
    {
        public GameObject Value;
    }
}