using Unity.Entities;
using UnityEngine;

namespace Components.ComponentsData
{
    public class DestroyFXPrefab : IComponentData
    {
        public GameObject Value;
    }
}