using Unity.Entities;

namespace Components.ComponentsData
{
    public class IsAttackEnableComponent : IComponentData
    {
        public IsAttackEnable Value;
    }
}