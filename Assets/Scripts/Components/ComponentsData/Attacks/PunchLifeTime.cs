using Unity.Entities;
using UnityEngine;

namespace Components.ComponentsData.Attacks
{
    public class PunchLifeTime : IComponentData
    {
        public float TimeToDestroy;
        public GameObject PositionGameObject;
    }
}