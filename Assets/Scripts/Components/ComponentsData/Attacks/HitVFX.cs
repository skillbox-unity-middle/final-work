using Unity.Entities;
using UnityEngine;

namespace Components.ComponentsData.Attacks
{
    public class HitVFX : IComponentData
    {
        public GameObject Value;
    }
}