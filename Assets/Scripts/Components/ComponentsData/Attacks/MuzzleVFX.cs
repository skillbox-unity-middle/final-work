using Unity.Entities;
using UnityEngine;

namespace Components.ComponentsData.Attacks
{
    public class MuzzleVFX : IComponentData
    {
        public GameObject Value;
    }
}