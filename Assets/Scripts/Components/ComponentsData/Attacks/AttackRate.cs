﻿using Unity.Entities;

namespace Components.ComponentsData.Attacks
{
    public struct AttackRate : IComponentData
    {
        public float RateOfFire;
        public float CurrentRateOfFire;
    }
}
