using Unity.Entities;

namespace Components.ComponentsData
{
    public class MeleeHitPointComponent : IComponentData
    {
        public MeleeHitPoints Value;
    }
}