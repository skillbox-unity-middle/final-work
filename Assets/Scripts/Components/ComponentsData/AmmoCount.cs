using Unity.Entities;

namespace Components.ComponentsData
{
    public struct AmmoCount : IComponentData
    {
        public int MaxAmmo;
        public int CurrentAmmo;
    }
}