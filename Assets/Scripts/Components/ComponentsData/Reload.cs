using Unity.Entities;

namespace Components.ComponentsData
{
    public struct Reload : IComponentData, IEnableableComponent
    {
        public float ReloadTime;
        public float CurrentReloadTime;
    }
}
