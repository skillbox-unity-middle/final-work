using Unity.Entities;

namespace Components.ComponentsData
{
    public class ShootPointComponent : IComponentData
    {
        public ShootPoint Value;
    }
}