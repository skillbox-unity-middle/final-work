using Unity.Entities;

namespace Components.ComponentsData
{
    public struct TriggerWithTarget : IComponentData, IEnableableComponent
    {
        public Entity Target;
    }
}