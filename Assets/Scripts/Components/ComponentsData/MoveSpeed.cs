﻿using Unity.Entities;

namespace Components.ComponentsData
{
    public struct MoveSpeed : IComponentData
    {
        public float Value;
    }
}