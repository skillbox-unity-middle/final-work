using UnityEngine;

namespace Components
{
    public class ShootPoint : MonoBehaviour
    {
        public GameObject ShootPointGameObject => shootPointGameObject;

        [SerializeField] private GameObject shootPointGameObject;
    }
}