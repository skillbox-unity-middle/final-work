using UnityEngine;

namespace Components
{
    public class MeleeHitPoints : MonoBehaviour
    {
        public GameObject LeftHandHitPoint => leftHandHitPoint;
        public GameObject RightHandHitPoint => rightHandHitPoint;

        [SerializeField] private GameObject leftHandHitPoint;
        [SerializeField] private GameObject rightHandHitPoint;
    }
}