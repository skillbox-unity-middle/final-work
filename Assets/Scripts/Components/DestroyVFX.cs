using UnityEngine;

namespace Components
{
    public class DestroyParticles : MonoBehaviour
    {
        [SerializeField] private float timeToDestroy;
        private void Start()
        {
            Destroy(gameObject, timeToDestroy);
        }
    }
}
