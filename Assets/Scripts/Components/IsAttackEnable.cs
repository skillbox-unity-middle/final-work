using UnityEngine;

namespace Components
{
    public class IsAttackEnable : MonoBehaviour
    {
        public bool Value
        {
            get => value;
            set => this.value = value;
        }

        [SerializeField] private bool value;
    }
}