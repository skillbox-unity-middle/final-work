using Components.ComponentsData.Spawners;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class EnemySpawnerAuthoring : MonoBehaviour
    {
        public GameObject enemyPrefab;
        public float timeToSpawn;

        public class EnemySpawnerBaker : Baker<EnemySpawnerAuthoring>
        {
            public override void Bake(EnemySpawnerAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);

                AddComponent(entity, new SpawnerEntityPrefab()
                {
                    Value = GetEntity(authoring.enemyPrefab, TransformUsageFlags.Dynamic)
                });

                AddComponent<EnemySpawnerTag>(entity);
            }
        }
    }
}