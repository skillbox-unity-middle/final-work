﻿using Components.ComponentsData.Attacks;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class DamageEntityAuthoring : MonoBehaviour
    {
        public GameObject hitVFX;
        
        public class DamageEntityBaker : Baker<DamageEntityAuthoring>
        {
            public override void Bake(DamageEntityAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                
                AddComponent<DamageEntityTag>(entity);
                
                AddComponentObject(entity, new HitVFX()
                {
                    Value = authoring.hitVFX
                });
            }
        }
    }
}