using Components.ComponentsData;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class MoveSpeedAuthoring : MonoBehaviour
    {
        public float moveSpeed;
        
        public class MoveSpeedBaker : Baker<MoveSpeedAuthoring>
        {
            public override void Bake(MoveSpeedAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);

                AddComponent(entity, new MoveSpeed()
                {
                    Value = authoring.moveSpeed
                });
            }
        }
    }
}