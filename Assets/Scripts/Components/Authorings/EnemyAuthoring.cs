using Components.ComponentsData;
using Components.ComponentsData.Attacks;
using Components.ComponentsData.Damage;
using Components.ComponentsData.Enemies;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class EnemyAuthoring : MonoBehaviour
    {
        public GameObject destroyFXPrefab;
        public GameObject spawnFXPrefab;
        
        public class  EnemyAuthoringBaker : Baker<EnemyAuthoring>
        {
            public override void Bake(EnemyAuthoring authoring)
            {
                var enemyEntity = GetEntity(TransformUsageFlags.Dynamic);
                
                AddComponent<EnemyTag>(enemyEntity);

                AddComponent(enemyEntity, new DamageSource()
                {
                    IsPlayer = false
                });
                
                AddComponent<AttackAllowedTag>(enemyEntity);
                SetComponentEnabled<AttackAllowedTag>(enemyEntity, false);

                AddComponentObject(enemyEntity, new DestroyFXPrefab()
                {
                    Value = authoring.destroyFXPrefab
                });               
                
                AddComponentObject(enemyEntity, new SpawnFXPrefab()
                {
                    Value = authoring.spawnFXPrefab
                });
            }
        }
    }
}