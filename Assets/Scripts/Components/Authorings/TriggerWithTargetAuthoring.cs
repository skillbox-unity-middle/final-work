using Components.ComponentsData;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class TriggerWithTargetAuthoring : MonoBehaviour
    {
        public class TriggerWithTargetBaker : Baker<TriggerWithTargetAuthoring>
        {
            public override void Bake(TriggerWithTargetAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                
                AddComponent<TriggerWithTarget>(entity);
                SetComponentEnabled<TriggerWithTarget>(entity, false);
            }
        }
    }
}