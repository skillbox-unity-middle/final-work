using Components.ComponentsData;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class HealthPointAuthoring : MonoBehaviour
    {
        public int maxHp;
        
        public class HealthPointBaker : Baker<HealthPointAuthoring>
        {
            public override void Bake(HealthPointAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                
                AddComponent(entity, new HealthPoint()
                {
                    MaxHp = authoring.maxHp,
                    CurrentHp = authoring.maxHp
                });
            }
        }
    }
}