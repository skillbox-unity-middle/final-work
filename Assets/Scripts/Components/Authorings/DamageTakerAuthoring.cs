using Components.ComponentsData.Damage;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class DamageTakerAuthoring : MonoBehaviour
    {
        public class DamageTakerBaker : Baker<DamageTakerAuthoring>
        {
            public override void Bake(DamageTakerAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);

                AddBuffer<DamageTaker>(entity);
                SetComponentEnabled<DamageTaker>(entity, false);
            }
        }
    }
}