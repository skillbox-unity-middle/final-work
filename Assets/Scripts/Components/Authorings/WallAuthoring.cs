using Components.ComponentsData;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class WallAuthoring : MonoBehaviour
    {
        public class WallBaker : Baker<WallAuthoring>
        {
            public override void Bake(WallAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);

                AddComponent<WallTag>(entity);
            }
        }
    }
}