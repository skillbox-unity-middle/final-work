using Components.ComponentsData;
using Components.ComponentsData.Attacks;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class AttackAuthoring : MonoBehaviour
    {
        public int maxAmmo;
        public float reloadTime;
        public float rateOfFire;
        
        public class AttackBaker : Baker<AttackAuthoring>
        {
            public override void Bake(AttackAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                
                AddComponent(entity, new AttackRate()
                {
                    RateOfFire = authoring.rateOfFire,
                    CurrentRateOfFire = 0
                });
                
                AddComponent(entity, new Reload()
                {
                    ReloadTime = authoring.reloadTime,
                    CurrentReloadTime = authoring.reloadTime
                });
                SetComponentEnabled<Reload>(entity, false);
                
                AddComponent(entity, new AmmoCount
                {
                    MaxAmmo =  authoring.maxAmmo,
                    CurrentAmmo = authoring.maxAmmo
                });
            }
        }
    }
}