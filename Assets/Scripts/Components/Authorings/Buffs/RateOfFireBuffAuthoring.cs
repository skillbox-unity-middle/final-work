using Components.ComponentsData.Buffs;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings.Buffs
{
    public class RateOfFireBuffAuthoring : MonoBehaviour
    {
        public float rateOfFireBuff;

        public class RateOfFireBuffBaker : Baker<RateOfFireBuffAuthoring>
        {
            public override void Bake(RateOfFireBuffAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);

                AddComponent(entity, new BuffComponent()
                {
                    Value = authoring.rateOfFireBuff,
                    StatToModify = StatTypes.RateOfFire
                });
            }
        }
    }
}