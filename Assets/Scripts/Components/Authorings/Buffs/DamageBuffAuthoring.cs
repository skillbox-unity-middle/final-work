using Components.ComponentsData.Buffs;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings.Buffs
{
    public class DamageBuffAuthoring : MonoBehaviour
    {
        public float damageBuff;
        
        public class DamageBuffBaker : Baker<DamageBuffAuthoring>
        {
            public override void Bake(DamageBuffAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);

                AddComponent(entity, new BuffComponent()
                {
                    Value = authoring.damageBuff,
                    StatToModify = StatTypes.Damage
                });
            }
        }
    }
}