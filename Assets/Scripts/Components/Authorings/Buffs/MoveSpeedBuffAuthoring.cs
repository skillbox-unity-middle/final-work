using Components.ComponentsData.Buffs;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings.Buffs
{
    public class MoveSpeedBuffAuthoring : MonoBehaviour
    {
        public float moveSpeedBuff;
        
        public class MoveSpeedBuffBaker : Baker<MoveSpeedBuffAuthoring>
        {
            public override void Bake(MoveSpeedBuffAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                
                AddComponent(entity, new BuffComponent()
                {
                    Value = authoring.moveSpeedBuff,
                    StatToModify = StatTypes.MoveSpeed
                });
            }
        }
    }
}