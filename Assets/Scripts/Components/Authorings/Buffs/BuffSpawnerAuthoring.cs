using Components.ComponentsData.Buffs;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings.Buffs
{
    public class BuffSpawnerAuthoring : MonoBehaviour
    {
        public GameObject rateOfFireBuffPrefab;
        public GameObject moveSpeedBuffPrefab;
        public GameObject healthPointBuffPrefab;
        public GameObject defenseBuffPrefab;
        public GameObject damageBuffPrefab;
        public float chanceToDrop;
        
        public class BuffSpawnerBaker : Baker<BuffSpawnerAuthoring>
        {
            public override void Bake(BuffSpawnerAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                
                AddComponent(entity, new BuffSpawner()
                {
                    RateOfFireBuffEntityPrefab = GetEntity(authoring.rateOfFireBuffPrefab, TransformUsageFlags.Dynamic),
                    MoveSpeedBuffEntityPrefab = GetEntity(authoring.moveSpeedBuffPrefab, TransformUsageFlags.Dynamic),
                    HealthPointBuffEntityPrefab = GetEntity(authoring.healthPointBuffPrefab, TransformUsageFlags.Dynamic),
                    DefenseBuffEntityPrefab = GetEntity(authoring.defenseBuffPrefab, TransformUsageFlags.Dynamic),
                    DamageBuffEntityPrefab = GetEntity(authoring.damageBuffPrefab, TransformUsageFlags.Dynamic),
                    ChanceToDrop = authoring.chanceToDrop
                });
            }
        }
    }
}