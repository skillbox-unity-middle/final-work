using Components.ComponentsData.Buffs;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings.Buffs
{
    public class HealthPointBuffAuthoring : MonoBehaviour
    {
        public int healthPoint;
        
        public class HealthPointBuffBaker : Baker<HealthPointBuffAuthoring>
        {
            public override void Bake(HealthPointBuffAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                
                AddComponent(entity, new BuffComponent()
                {
                    Value = authoring.healthPoint,
                    StatToModify = StatTypes.HealthPoint
                });
            }
        }
    }
}