using Components.ComponentsData.Buffs;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings.Buffs
{
    public class DefenseBuffAuthoring : MonoBehaviour
    {
        public float defenseBuff;
        
        public class DefenseBuffBaker : Baker<DefenseBuffAuthoring>
        {
            public override void Bake(DefenseBuffAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);

                AddComponent(entity, new BuffComponent()
                {
                    Value = authoring.defenseBuff,
                    StatToModify = StatTypes.Defense
                });
            }
        }
    }
}