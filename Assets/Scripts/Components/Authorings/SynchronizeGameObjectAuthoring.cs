using Components.ComponentsData;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class SynchronizeGameObjectAuthoring : MonoBehaviour
    {
        public GameObject gameObjectPrefab;
        
        public class SynchronizeGameObjectBaker : Baker<SynchronizeGameObjectAuthoring>
        {
            public override void Bake(SynchronizeGameObjectAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                
                AddComponentObject(entity, new SynchronizeGameObjectPrefab
                {
                    Value = authoring.gameObjectPrefab
                });
            }
        }
    }
}