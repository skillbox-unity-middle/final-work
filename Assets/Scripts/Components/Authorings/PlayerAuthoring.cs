﻿using Components.ComponentsData;
using Components.ComponentsData.Attacks;
using Components.ComponentsData.Damage;
using Components.ComponentsData.Player;
using Components.ComponentsData.Spawners;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class PlayerAuthoring : MonoBehaviour
    {
        public GameObject projectilePrefab;
        public GameObject muzzlePrefab;
        public LayerMask groundLayerMask;
        
        public class PlayerAuthoringBaker : Baker<PlayerAuthoring>
        {
            public override void Bake(PlayerAuthoring authoring)
            {
                var playerEntity = GetEntity(TransformUsageFlags.Dynamic);
                const float startDefense = 1.0f;
                const float startDamage = 1.0f;
                
                AddComponent<PlayerTag>(playerEntity);
                AddComponent<PlayerMoveInput>(playerEntity);
                AddComponent<PlayerRotateInput>(playerEntity);
                AddComponent(playerEntity, new GroundLayerMask()
                {
                    Value = authoring.groundLayerMask
                });
                
                AddComponent<AttackAllowedTag>(playerEntity);
                SetComponentEnabled<AttackAllowedTag>(playerEntity, false);
            
                AddComponent<CameraFollowTag>(playerEntity);
                SetComponentEnabled<CameraFollowTag>(playerEntity, true);

                AddComponent(playerEntity, new SpawnerEntityPrefab
                {
                    Value = GetEntity(authoring.projectilePrefab, TransformUsageFlags.Dynamic)
                });
                
                AddComponentObject(playerEntity, new MuzzleVFX()
                {
                    Value = authoring.muzzlePrefab
                });
                
                AddComponent(playerEntity, new DamageSource()
                {
                    IsPlayer = true
                });
                
                AddComponent(playerEntity, new DefenseComponent()
                {
                    Value = startDefense
                });
                
                AddComponent(playerEntity, new DamageMultiplierComponent()
                {
                    Value = startDamage
                });
            }
        }
    }
}