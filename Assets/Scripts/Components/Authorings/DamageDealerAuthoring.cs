using Components.ComponentsData.Damage;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class DamageDealerAuthoring : MonoBehaviour
    {
        public int damage;
        
        public class DamageDealerBaker : Baker<DamageDealerAuthoring>
        {
            public override void Bake(DamageDealerAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);

                AddComponent(entity, new DamageDealer()
                {
                    Value = authoring.damage
                });
            }
        }
    }
}