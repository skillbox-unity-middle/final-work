using Components.ComponentsData.Spawners;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class MeleeEnemyAuthoring : MonoBehaviour
    {
        public GameObject punchPrefab;
        
        public class RangeEnemyBaker : Baker<MeleeEnemyAuthoring>
        {
            public override void Bake(MeleeEnemyAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);

                AddComponent(entity, new SpawnerEntityPrefab()
                {
                    Value = GetEntity(authoring.punchPrefab, TransformUsageFlags.Dynamic)
                });
            }
        }
    }
}