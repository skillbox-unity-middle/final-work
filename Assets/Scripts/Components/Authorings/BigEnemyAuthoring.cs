using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class BigEnemyAuthoring : MonoBehaviour
    {
        public class RangeEnemyBaker : Baker<BigEnemyAuthoring>
        {
            public override void Bake(BigEnemyAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
            }
        }
    }
}