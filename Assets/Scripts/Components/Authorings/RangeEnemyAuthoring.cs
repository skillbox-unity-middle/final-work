using Components.ComponentsData.Attacks;
using Components.ComponentsData.Spawners;
using Unity.Entities;
using UnityEngine;

namespace Components.Authorings
{
    public class RangeEnemyAuthoring : MonoBehaviour
    {
        public GameObject projectilePrefab;
        public GameObject muzzlePrefab;
        
        public class RangeEnemyBaker : Baker<RangeEnemyAuthoring>
        {
            public override void Bake(RangeEnemyAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                
                AddComponent(entity, new SpawnerEntityPrefab()
                {
                    Value = GetEntity(authoring.projectilePrefab, TransformUsageFlags.Dynamic)
                });
                
                AddComponentObject(entity, new MuzzleVFX()
                {
                    Value = authoring.muzzlePrefab
                });
            }
        }
    }
}