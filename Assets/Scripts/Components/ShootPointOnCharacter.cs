using UnityEngine;

namespace Components
{
    public class ShootPointOnCharacter : MonoBehaviour
    {
        public ShootPoint ShootPointValue => shootPoint;

        [SerializeField] private ShootPoint shootPoint;
    }
}