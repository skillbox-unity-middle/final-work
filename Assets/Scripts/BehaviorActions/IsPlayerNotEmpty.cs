using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

namespace BehaviorActions
{
    public class IsGameObjectNotEmpty : Action
    {
        public SharedGameObject sharedGameObject;

        public override TaskStatus OnUpdate()
        {
            return sharedGameObject.Value == null ? TaskStatus.Running : TaskStatus.Success;
        }
    }
}