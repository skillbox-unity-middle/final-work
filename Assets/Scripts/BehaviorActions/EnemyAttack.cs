using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Components;

namespace BehaviorActions
{
    public class EnemyAttack : Action
    {
        public SharedBool IsAttackEnable;
        
        private IsAttackEnable _enemy;
        
        public override void OnAwake()
        {
            _enemy = gameObject.GetComponent<IsAttackEnable>();
        }

        public override TaskStatus OnUpdate()
        {
            if (_enemy == null) return TaskStatus.Failure;

            _enemy.Value = IsAttackEnable.Value;
            
            return TaskStatus.Success;
        }
    }
}