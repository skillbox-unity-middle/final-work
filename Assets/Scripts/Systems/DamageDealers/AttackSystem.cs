using System.Runtime.InteropServices;
using Components.ComponentsData;
using Components.ComponentsData.Attacks;
using Components.ComponentsData.Damage;
using Components.ComponentsData.Player;
using Components.ComponentsData.Spawners;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Systems.DamageDealers
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(TransformSystemGroup))]
    [StructLayout(LayoutKind.Auto)]
    public partial struct AttackSystem : ISystem
    {
        private ComponentLookup<DamageMultiplierComponent> _damageMultipliers;

        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            _damageMultipliers = SystemAPI.GetComponentLookup<DamageMultiplierComponent>();
        }

        public void OnUpdate(ref SystemState state)
        {
            _damageMultipliers.Update(ref state);
            var ecb = new EntityCommandBuffer(Allocator.Temp);
            
            //MeleeEnemiesPunch
            foreach (var (punchPrefab, punch, hitPoints, damageSource, entity) in 
                     SystemAPI.Query<SpawnerEntityPrefab, RefRW<AttackRate>, MeleeHitPointComponent, 
                         DamageSource>().WithAll<AttackAllowedTag, LocalTransform>().WithNone<Reload>().WithEntityAccess())
            {
                punch.ValueRW.CurrentRateOfFire -= SystemAPI.Time.DeltaTime;
                if(punch.ValueRO.CurrentRateOfFire > 0) continue;
                
                var newLeftPunch = ecb.Instantiate(punchPrefab.Value);
                var newLeftTransform = hitPoints.Value.LeftHandHitPoint.transform;
                ecb.SetComponent(newLeftPunch, LocalTransform.FromPositionRotationScale
                    (newLeftTransform.position, newLeftTransform.rotation, 1.0f));
                
                ecb.AddComponent(newLeftPunch, new DamageSource()
                {
                    IsPlayer = damageSource.IsPlayer
                });
                
                var newRightPunch = ecb.Instantiate(punchPrefab.Value);
                var newRightTransform = hitPoints.Value.RightHandHitPoint.transform;
                ecb.SetComponent(newRightPunch, LocalTransform.FromPositionRotationScale
                    (newRightTransform.position, newRightTransform.rotation, 1.0f));
                
                ecb.AddComponent(newRightPunch, new DamageSource()
                {
                    IsPlayer = damageSource.IsPlayer
                });
                
                var damageMultiplier = 1.0f;
            
                if (_damageMultipliers.HasComponent(entity))
                    damageMultiplier = _damageMultipliers.GetRefRO(entity).ValueRO.Value;
                
                ecb.AddComponent(newLeftPunch, new DamageMultiplierComponent()
                {
                    Value = damageMultiplier
                });
                
                ecb.AddComponent(newRightPunch, new DamageMultiplierComponent()
                {
                    Value = damageMultiplier
                });
                
                ecb.AddComponent(newLeftPunch, new PunchLifeTime()
                {
                    TimeToDestroy = punch.ValueRO.RateOfFire,
                    PositionGameObject = hitPoints.Value.LeftHandHitPoint
                });
                
                ecb.AddComponent(newRightPunch, new PunchLifeTime()
                {
                    TimeToDestroy = punch.ValueRO.RateOfFire,
                    PositionGameObject = hitPoints.Value.RightHandHitPoint
                });
                
                punch.ValueRW.CurrentRateOfFire = punch.ValueRO.RateOfFire;
            }
            
            foreach (var (projectilePrefab, transform, ammoCount, fire, muzzleVFX, 
                         shootPointComponent, damageSource, entity) in SystemAPI.Query<SpawnerEntityPrefab, 
                             LocalTransform, RefRW<AmmoCount>, RefRW<AttackRate>, MuzzleVFX, ShootPointComponent, 
                             DamageSource>().WithAll<AttackAllowedTag>().WithNone<Reload>().WithEntityAccess())
            {
                fire.ValueRW.CurrentRateOfFire -= SystemAPI.Time.DeltaTime;
                if(fire.ValueRO.CurrentRateOfFire > 0) continue;
                
                var newProjectile = ecb.Instantiate(projectilePrefab.Value);

                var newTransform = shootPointComponent.Value.ShootPointGameObject.transform;
                var newPosition = newTransform.position;
                var projectileTransform = LocalTransform.FromPositionRotationScale
                    (newPosition, transform.Rotation, 0.5f);

                ecb.SetComponent(newProjectile, projectileTransform);
            
                ecb.AddComponent(newProjectile, new DamageSource()
                {
                    IsPlayer = damageSource.IsPlayer
                });
                
                Object.Instantiate(muzzleVFX.Value, newPosition, transform.Rotation);
                
                var damageMultiplier = 1.0f;

                if (_damageMultipliers.HasComponent(entity))
                    damageMultiplier = _damageMultipliers.GetRefRO(entity).ValueRO.Value;
                
                ecb.AddComponent(newProjectile, new DamageMultiplierComponent()
                {
                    Value = damageMultiplier
                });
                
                ammoCount.ValueRW.CurrentAmmo -= 1;
            
                if(ammoCount.ValueRO.CurrentAmmo == 0) ecb.SetComponentEnabled<Reload>(entity, true);
                
                fire.ValueRW.CurrentRateOfFire = fire.ValueRO.RateOfFire;
            }

            foreach (var fire in SystemAPI.Query<RefRW<AttackRate>>().WithNone<AttackAllowedTag>())
            {
                if (fire.ValueRO.CurrentRateOfFire <= 0) continue;
                fire.ValueRW.CurrentRateOfFire -= SystemAPI.Time.DeltaTime;
            }

            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }
    }
}
