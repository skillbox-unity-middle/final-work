using Components.ComponentsData;
using Components.ComponentsData.Attacks;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;

namespace Systems.DamageDealers
{
    [UpdateAfter(typeof(AttackSystem))]
    public partial struct PunchLifetimeSystem : ISystem
    {
        public void OnUpdate(ref SystemState state)
        {
            var ecb = new EntityCommandBuffer(Allocator.Temp);
            
            var deltaTime = SystemAPI.Time.DeltaTime;

            foreach (var (transform, punchLifeTime, entity) 
                     in SystemAPI.Query<RefRW<LocalTransform>, PunchLifeTime>().WithEntityAccess())
            {
                punchLifeTime.TimeToDestroy -= deltaTime;

                if (punchLifeTime.PositionGameObject.Equals(null))
                {
                    ecb.AddComponent<DestroyWithoutFXTag>(entity);
                    continue;
                }
                
                transform.ValueRW.Position = punchLifeTime.PositionGameObject.transform.position;
                
                if (punchLifeTime.TimeToDestroy <= 0) ecb.AddComponent<DestroyWithoutFXTag>(entity);
            }

            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }
    }
}