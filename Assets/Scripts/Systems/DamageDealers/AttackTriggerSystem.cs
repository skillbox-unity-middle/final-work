using System.Runtime.InteropServices;
using Components.ComponentsData;
using Components.ComponentsData.Attacks;
using Components.ComponentsData.Damage;
using Components.ComponentsData.Enemies;
using Components.ComponentsData.Player;
using Unity.Burst;
using Unity.Entities;
using UnityEngine;

namespace Systems.DamageDealers
{
    [BurstCompile]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(DestroyEntitySystem))]
    [StructLayout(LayoutKind.Auto)]
    public partial struct AttackTriggerSystem : ISystem
    {
        private ComponentLookup<EnemyTag> _enemies;
        private ComponentLookup<PlayerTag> _players;
        private ComponentLookup<WallTag> _walls;
        
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<BeginSimulationEntityCommandBufferSystem.Singleton>();

            _enemies = SystemAPI.GetComponentLookup<EnemyTag>();
            _players = SystemAPI.GetComponentLookup<PlayerTag>();
            _walls = SystemAPI.GetComponentLookup<WallTag>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            _players.Update(ref state);
            _enemies.Update(ref state);
            _walls.Update(ref state);
            var ecb = SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>()
                .CreateCommandBuffer(state.WorldUnmanaged);
                
            new ProjectileTriggerJob
            {
                Enemies = _enemies,
                Players = _players,
                Walls = _walls,
                ECB = ecb
            }.Schedule();
        }
        
        [BurstCompile]
        [WithAll(typeof(DamageEntityTag))]
        [StructLayout(LayoutKind.Auto)]
        public partial struct ProjectileTriggerJob : IJobEntity
        {
            public ComponentLookup<EnemyTag> Enemies;
            public ComponentLookup<PlayerTag> Players;
            public ComponentLookup<WallTag> Walls;
            public EntityCommandBuffer ECB;
            
            private void Execute(ref TriggerWithTarget triggerWithTarget, ref DamageSource damageSource,
                in DamageDealer damageDealer, in DamageMultiplierComponent damageMultiplier, Entity entity)
            {
                ECB.SetComponentEnabled<TriggerWithTarget>(entity, false);
                
                if (!Walls.HasComponent(triggerWithTarget.Target))
                {
                    if (!Enemies.HasComponent(triggerWithTarget.Target) && damageSource.IsPlayer) 
                        return;
                    if (!Players.HasComponent(triggerWithTarget.Target) && !damageSource.IsPlayer) 
                        return;
                    var newDamage = (int)(damageMultiplier.Value * damageDealer.Value);
                
                    ECB.SetComponentEnabled<DamageTaker>(triggerWithTarget.Target, true);
                    ECB.AppendToBuffer(triggerWithTarget.Target, new DamageTaker()
                    {
                        Value = newDamage
                    });
                }
                
                ECB.RemoveComponent<DamageSource>(entity);
                
                ECB.AddComponent(entity, new DestroyTag());
            }
        }
    }
}