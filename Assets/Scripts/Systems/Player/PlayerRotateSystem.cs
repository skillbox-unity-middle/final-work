﻿using Components.ComponentsData.Player;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Systems.Player
{
    [UpdateBefore(typeof(TransformSystemGroup))]
    [UpdateAfter(typeof(PlayerMoveSystem))]
    public partial class PlayerRotateSystem : SystemBase
    {
        private Camera _camera;

        protected override void OnCreate()
        {
            RequireForUpdate<PlayerRotateInput>();
            RequireForUpdate<PlayerTag>();
        }

        protected override void OnStartRunning()
        {
            _camera = Camera.main;
        }

        protected override void OnUpdate()
        {
            foreach (var (transform, rotateInput, groundLayerMask) in
                     SystemAPI.Query<RefRW<LocalTransform>, PlayerRotateInput, GroundLayerMask>())
            {
                if (!(math.lengthsq(rotateInput.Delta) > float.Epsilon)) continue;

                var ray = _camera.ScreenPointToRay(new Vector2(rotateInput.Value.x, rotateInput.Value.y));

                if (!Physics.Raycast(ray, out var raycastHit, float.MaxValue, groundLayerMask.Value)) continue;
                var newFloat = new float3(raycastHit.point.x, transform.ValueRO.Position.y, raycastHit.point.z);
                var newPos = newFloat - transform.ValueRO.Position;
                transform.ValueRW.Rotation = quaternion.LookRotation(newPos, math.up());


            }
        }
    }
}
