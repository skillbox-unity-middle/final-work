using Components.ComponentsData;
using Systems.DamageDealers;
using Unity.Burst;
using Unity.Entities;

namespace Systems.Player
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(AttackSystem))]
    public partial struct ReloadSystem : ISystem
    {
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            foreach (var (ammo,reload) in SystemAPI.Query<RefRW<AmmoCount>, RefRW<Reload>>())
            {
                reload.ValueRW.CurrentReloadTime -= SystemAPI.Time.DeltaTime;

                if (reload.ValueRW.CurrentReloadTime <= 0)
                {
                    ammo.ValueRW.CurrentAmmo = ammo.ValueRO.MaxAmmo;
                }
            }
        }
    }
}
