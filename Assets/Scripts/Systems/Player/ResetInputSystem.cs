﻿using Components.ComponentsData;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;

namespace Systems.Player
{
    [UpdateInGroup(typeof(SimulationSystemGroup), OrderLast = true)]
    [UpdateBefore(typeof(DestroyEntitySystem))]
    [UpdateAfter(typeof(EndSimulationEntityCommandBufferSystem))]
    public partial struct ResetInputSystem : ISystem
    {
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecb = new EntityCommandBuffer(Allocator.Temp);
        
            foreach (var (reload, entity) in SystemAPI.Query<RefRW<Reload>>().WithEntityAccess())
            {
                if (!(reload.ValueRO.CurrentReloadTime <= 0)) continue;
                reload.ValueRW.CurrentReloadTime = reload.ValueRO.ReloadTime;
                ecb.SetComponentEnabled<Reload>(entity, false);
            }

            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }
    }
}