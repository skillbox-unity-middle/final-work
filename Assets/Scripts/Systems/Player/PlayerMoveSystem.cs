﻿using System.Runtime.InteropServices;
using Components.ComponentsData;
using Components.ComponentsData.Player;
using Unity.Burst;
using Unity.Entities;
using Unity.Transforms;

namespace Systems.Player
{
    [BurstCompile]
    [UpdateBefore(typeof(TransformSystemGroup))]
    public partial struct PlayerMoveSystem : ISystem
    {
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var deltaTime = SystemAPI.Time.DeltaTime;

            new MoveJob()
            {
                DeltaTime = deltaTime
            }.Schedule();
        }
        
        [BurstCompile]
        [StructLayout(LayoutKind.Auto)]
        public partial struct MoveJob : IJobEntity
        {
            public float DeltaTime;
            
            [BurstCompile]
            private void Execute(ref LocalTransform transform, in PlayerMoveInput moveInput, in MoveSpeed moveSpeed)
            {
                transform.Position.xz += moveInput.Value * moveSpeed.Value * DeltaTime;
            }
        }
    }
}