using System.Runtime.InteropServices;
using Components.ComponentsData;
using Components.ComponentsData.Attacks;
using Components.ComponentsData.Enemies;
using Components.ComponentsData.Player;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Systems.Player
{
    [BurstCompile]
    [UpdateAfter(typeof(PlayerMoveSystem))]
    [StructLayout(LayoutKind.Auto)]
    public partial struct AnimatorSystem : ISystem
    {
        private static readonly int Speed = Animator.StringToHash("Speed");
        private static readonly int Fire = Animator.StringToHash("Fire");
        private static readonly int Reload = Animator.StringToHash("Reload");
        private static readonly int XInput = Animator.StringToHash("XInput");
        private static readonly int YInput = Animator.StringToHash("YInput");
        private ComponentLookup<AttackAllowedTag> _fireAllowedTags;
        private ComponentLookup<Reload> _reloads;

        public void OnCreate(ref SystemState state)
        {
            _fireAllowedTags = SystemAPI.GetComponentLookup<AttackAllowedTag>();
            _reloads = SystemAPI.GetComponentLookup<Reload>();
        }
        
        public void OnUpdate(ref SystemState state)
        {
            _fireAllowedTags.Update(ref state);
            _reloads.Update(ref state);
            foreach (var (transform, animatorReference, moveInput, entity) in SystemAPI.Query<LocalTransform, AnimatorReference, PlayerMoveInput>()
                         .WithAll<AttackAllowedTag, Reload>().WithOptions(EntityQueryOptions.IgnoreComponentEnabledState).WithEntityAccess())
            {
                animatorReference.Value.SetFloat(Speed, math.length(moveInput.Value));
                
                var radians = GetQuaternionEulerAngles(transform.Rotation.value).y;
                
                animatorReference.Value.SetFloat(XInput, moveInput.Value.x * math.cos(radians) + moveInput.Value.y * math.sin(radians));
                animatorReference.Value.SetFloat(YInput, moveInput.Value.x * math.sin(radians) + moveInput.Value.y * math.cos(radians));
                
                var reload = 0.0f;   
                if (_reloads.IsComponentEnabled(entity)) reload = 1.0f;
                animatorReference.Value.SetFloat(Reload, reload);
                
                var fire = 0.0f;
                if (_fireAllowedTags.IsComponentEnabled(entity)) fire = 1.0f;
                animatorReference.Value.SetFloat(Fire, fire);   
            }

            foreach (var (animatorReference, entity) in SystemAPI.Query<AnimatorReference>()
                         .WithAll<AttackAllowedTag, EnemyTag>().WithOptions(EntityQueryOptions.IgnoreComponentEnabledState).WithEntityAccess())
            {
                var fire = 0.0f;
                if (_fireAllowedTags.IsComponentEnabled(entity)) fire = 1.0f;
                animatorReference.Value.SetFloat(Fire, fire);  
            }
            
            foreach (var (animatorReference, entity) in SystemAPI.Query<AnimatorReference>()
                         .WithAll<AttackAllowedTag, Reload, EnemyTag>().WithOptions(EntityQueryOptions.IgnoreComponentEnabledState).WithEntityAccess())
            {
                var reload = 0.0f;   
                if (_reloads.IsComponentEnabled(entity)) reload = 1.0f;
                animatorReference.Value.SetFloat(Reload, reload);
            }
        }
        
        private static float3 GetQuaternionEulerAngles(quaternion rot)
        {
            var q1 = rot.value;
            var sqw = q1.w * q1.w;
            var sqx = q1.x * q1.x;
            var sqy = q1.y * q1.y;
            var sqz = q1.z * q1.z;
            var unit = sqx + sqy + sqz + sqw;
            var test = q1.x * q1.w - q1.y * q1.z;
            float3 v;
 
            if (test > 0.4995f * unit)
            { 
                v.y = 2f * math.atan2(q1.y, q1.x);
                v.x = math.PI / 2f;
                v.z = 0;
                return NormalizeAngles(v);
            }
            if (test < -0.4995f * unit)
            { 
                v.y = -2f * math.atan2(q1.y, q1.x);
                v.x = -math.PI / 2;
                v.z = 0;
                return NormalizeAngles(v);
            }
 
            rot = new quaternion(q1.w, q1.z, q1.x, q1.y);
            v.y = math.atan2(2f * rot.value.x * rot.value.w + 2f * rot.value.y * rot.value.z, 1 - 2f * (rot.value.z * rot.value.z + rot.value.w * rot.value.w));     // Yaw
            v.x = math.asin(2f * (rot.value.x * rot.value.z - rot.value.w * rot.value.y));                             // Pitch
            v.z = math.atan2(2f * rot.value.x * rot.value.y + 2f * rot.value.z * rot.value.w, 1 - 2f * (rot.value.y * rot.value.y + rot.value.z * rot.value.z));      // Roll
            return NormalizeAngles(v);
        }

        private static float3 NormalizeAngles(float3 angles)
        {
            angles.x = NormalizeAngle(angles.x);
            angles.y = NormalizeAngle(angles.y);
            angles.z = NormalizeAngle(angles.z);
            return angles;
        }

        private static float NormalizeAngle(float angle)
        {
            while (angle > math.PI * 2f)
                angle -= math.PI * 2f;
            while (angle < 0)
                angle += math.PI * 2f;
            return angle;
        }
    }
}