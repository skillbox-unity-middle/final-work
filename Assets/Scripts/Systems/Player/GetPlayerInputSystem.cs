using Components.ComponentsData;
using Components.ComponentsData.Attacks;
using Components.ComponentsData.Player;
using Unity.Entities;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Systems.Player
{
    [UpdateInGroup(typeof(InitializationSystemGroup), OrderLast = true)]
    public partial class GetPlayerInputSystem : SystemBase
    {
        private PlayerActions _playerActions;
        private Entity _playerEntity;

        protected override void OnCreate()
        {
            RequireForUpdate<PlayerTag>();
            RequireForUpdate<PlayerMoveInput>();
            RequireForUpdate<PlayerRotateInput>();

            _playerActions = new PlayerActions();
        }

        protected override void OnStartRunning()
        {
            _playerActions.Enable();
            _playerActions.PlayerActionsMap.PlayerShoot.performed += OnPlayerShoot;
            _playerActions.PlayerActionsMap.PlayerShoot.canceled += OnPlayerShootCancel;
            _playerActions.PlayerActionsMap.PlayerReload.performed += OnPlayerReload;

            _playerEntity = SystemAPI.GetSingletonEntity<PlayerTag>();
        }

        protected override void OnUpdate()
        {
            var curMoveInput = _playerActions.PlayerActionsMap.PlayerMovement.ReadValue<Vector2>();
            var curRotateInput = _playerActions.PlayerActionsMap.MousePosition.ReadValue<Vector2>();
            var curDeltaMouseInput = _playerActions.PlayerActionsMap.MouseDelta.ReadValue<Vector2>();
            SystemAPI.SetSingleton(new PlayerMoveInput { Value = curMoveInput });
            SystemAPI.SetSingleton(new PlayerRotateInput {
                Value = curRotateInput,
                Delta = curDeltaMouseInput
            });
        }

        protected override void OnStopRunning()
        {
            _playerActions.PlayerActionsMap.PlayerShoot.performed -= OnPlayerShoot;
            _playerActions.PlayerActionsMap.PlayerReload.performed -= OnPlayerReload;
            _playerActions.Disable();

            _playerEntity = Entity.Null;
        }

        private void OnPlayerShoot(InputAction.CallbackContext obj)
        {
            if (!SystemAPI.Exists(_playerEntity)) return;

            SystemAPI.SetComponentEnabled<AttackAllowedTag>(_playerEntity, true);
        }
    
        private void OnPlayerShootCancel(InputAction.CallbackContext obj)
        {
            if (!SystemAPI.Exists(_playerEntity)) return;

            SystemAPI.SetComponentEnabled<AttackAllowedTag>(_playerEntity, false);
        }
    
        private void OnPlayerReload(InputAction.CallbackContext obj)
        {
            if (!SystemAPI.Exists(_playerEntity)) return;

            SystemAPI.SetComponentEnabled<Reload>(_playerEntity, true);
        }
    }
}
