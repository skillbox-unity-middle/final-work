using Cinemachine;
using Components.ComponentsData;
using Components.ComponentsData.Player;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Systems.Player
{
    [UpdateInGroup(typeof(PresentationSystemGroup), OrderLast = true)]
    public partial class UpdateCinemachineVirtualCameraSystem : SystemBase
    {
        private ICinemachineCamera _cinemachineCamera;

        protected override void OnCreate()
        {
            RequireForUpdate<SynchronizeGameObject>();
        }

        protected override void OnStartRunning()
        {
            if (Camera.main != null)
                _cinemachineCamera = Camera.main.GetComponent<CinemachineBrain>().ActiveVirtualCamera;
        }

        protected override void OnUpdate()
        {
            var ecb = new EntityCommandBuffer(Allocator.Temp);
            
            foreach (var (playerGameObject, entity) in SystemAPI.Query<SynchronizeGameObject>().WithAll<CameraFollowTag>().WithEntityAccess())
            {
                _cinemachineCamera.Follow = playerGameObject.Value.transform;
                ecb.SetComponentEnabled<CameraFollowTag>(entity, false);
            }

            ecb.Playback(EntityManager);
            ecb.Dispose();
        }
    }
}