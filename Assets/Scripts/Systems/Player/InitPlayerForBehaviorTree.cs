using System.Runtime.InteropServices;
using BehaviorDesigner.Runtime;
using Components.ComponentsData;
using Components.ComponentsData.Enemies;
using Components.ComponentsData.Player;
using Systems.Spawners;
using Unity.Collections;
using Unity.Entities;

namespace Systems.Player
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(EnemySpawnerSystem))]
    [StructLayout(LayoutKind.Auto)]
    public partial struct InitPlayerForBehaviorTree : ISystem
    {
       
        public void OnUpdate(ref SystemState state)
        {
            var ecb = new EntityCommandBuffer(Allocator.Temp);

            foreach (var (synchronizeGameObject, entity) in SystemAPI.Query<SynchronizeGameObject>().WithNone<IsInitTag>().WithAll<EnemyTag>().WithEntityAccess())
            {
                if (synchronizeGameObject == null) continue;

                ecb.AddComponent<IsInitTag>(entity);
                
                foreach (var playerGameObject in SystemAPI.Query<SynchronizeGameObject>().WithAll<PlayerTag>())
                {
                    var behaviorTree = synchronizeGameObject.Value.GetComponent<BehaviorTree>();
                    behaviorTree.SetVariableValue("Player", playerGameObject.Value);
                }
            }

            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }
    }
}