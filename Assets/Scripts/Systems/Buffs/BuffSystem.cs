using System.Runtime.InteropServices;
using Components.ComponentsData;
using Components.ComponentsData.Attacks;
using Components.ComponentsData.Buffs;
using Components.ComponentsData.Player;
using Unity.Burst;
using Unity.Entities;

namespace Systems.Buffs
{
    [BurstCompile]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateAfter(typeof(PickUpBuffSystem))]
    [UpdateBefore(typeof(DestroyEntitySystem))]
    public partial struct BuffSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<BeginSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecb = SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>()
                .CreateCommandBuffer(state.WorldUnmanaged);
            
            new RateOfFireBuffJob
            {
                ECB = ecb
            }.Schedule();
        }
        
        [BurstCompile]
        [WithAll(typeof(PlayerTag))]
        [StructLayout(LayoutKind.Auto)]
        public partial struct RateOfFireBuffJob : IJobEntity
        {
            public EntityCommandBuffer ECB;
            
            [BurstCompile]
            private void Execute(ref BuffComponent buffComponent, Entity entity, 
                ref AttackRate attackRate, ref MoveSpeed moveSpeed, ref HealthPoint healthPoint,
                ref DefenseComponent defenseComponent, ref DamageMultiplierComponent damageMultiplier)
            {
                switch (buffComponent.StatToModify)
                {
                    case StatTypes.MoveSpeed:
                        moveSpeed.Value *= buffComponent.Value;
                        break;
                    case StatTypes.RateOfFire:
                        attackRate.RateOfFire *= buffComponent.Value;
                        break;
                    case StatTypes.HealthPoint:
                        var tempHp = healthPoint.CurrentHp + (int)buffComponent.Value;
                        if (tempHp > healthPoint.MaxHp) healthPoint.CurrentHp = healthPoint.MaxHp;
                        else healthPoint.CurrentHp += (int)buffComponent.Value;
                        break;
                    case StatTypes.Defense:
                        defenseComponent.Value -= defenseComponent.Value * buffComponent.Value;
                        break;
                    case StatTypes.Damage:
                        damageMultiplier.Value += buffComponent.Value;
                        break;
                    case StatTypes.None:
                        break;
                }
                
                ECB.RemoveComponent<BuffComponent>(entity);
            }
        }
    }
}