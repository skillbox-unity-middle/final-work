using System.Runtime.InteropServices;
using Components.ComponentsData;
using Components.ComponentsData.Buffs;
using Components.ComponentsData.Player;
using Unity.Burst;
using Unity.Entities;

namespace Systems.Buffs
{
    [BurstCompile]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(DestroyEntitySystem))]
    [StructLayout(LayoutKind.Auto)]
    public partial struct PickUpBuffSystem : ISystem
    {
        private ComponentLookup<PlayerTag> _players;

        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<BeginSimulationEntityCommandBufferSystem.Singleton>();
            _players = state.GetComponentLookup<PlayerTag>();
        }
        
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            _players.Update(ref state);
            
            var ecb = SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>()
                .CreateCommandBuffer(state.WorldUnmanaged);
            
            new ProjectileTriggerJob
            {
                Players = _players,
                ECB = ecb
            }.Schedule();
        }

        [BurstCompile]
        [StructLayout(LayoutKind.Auto)]
        public partial struct ProjectileTriggerJob : IJobEntity
        {
            public ComponentLookup<PlayerTag> Players;
            public EntityCommandBuffer ECB;
            
            [BurstCompile]
            private void Execute(ref BuffComponent buffComponent, ref TriggerWithTarget triggerWithTarget, Entity entity)
            {
                ECB.SetComponentEnabled<TriggerWithTarget>(entity, false);
                if (!Players.HasComponent(triggerWithTarget.Target)) return;
                
                ECB.AddComponent(triggerWithTarget.Target, new BuffComponent()
                {
                    Value = buffComponent.Value,
                    StatToModify = buffComponent.StatToModify
                });

                ECB.AddComponent(entity, new DestroyTag());
            }
        }
    }
}