﻿using Components;
using Components.ComponentsData;
using Components.ComponentsData.Attacks;
using Components.ComponentsData.Enemies;
using Components.ComponentsData.Player;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Systems
{
    [UpdateInGroup(typeof(PresentationSystemGroup), OrderFirst = true)]
    public partial struct SynchronizeGameObjectSystem : ISystem
    {
        public void OnUpdate(ref SystemState state)
        {
            var ecb = new EntityCommandBuffer(Allocator.Temp);
            
            foreach (var (syncGameObjectPrefab, localTransform, entity) in
                     SystemAPI.Query<SynchronizeGameObjectPrefab, LocalTransform>().WithNone<SynchronizeGameObject>().WithEntityAccess())
            {
                var newCompanionGameObject = Object.Instantiate(syncGameObjectPrefab.Value);
                newCompanionGameObject.transform.position = localTransform.Position;
                newCompanionGameObject.transform.rotation = localTransform.Rotation;

                ecb.AddComponent(entity, new SynchronizeGameObject
                {
                    Value = newCompanionGameObject
                });

                if (newCompanionGameObject.TryGetComponent(out ShootPointOnCharacter shootPointOnCharacter))
                {
                    ecb.AddComponent(entity, new ShootPointComponent()
                    {
                        Value = shootPointOnCharacter.ShootPointValue
                    });
                }
                
                if (newCompanionGameObject.TryGetComponent(out MeleeHitPoints meleeHitPoints))
                {
                    ecb.AddComponent(entity, new MeleeHitPointComponent()
                    {
                        Value = meleeHitPoints
                    });
                }
                
                var isAttackEnableComponent = newCompanionGameObject.GetComponent<IsAttackEnable>();
                if (isAttackEnableComponent == null) continue;
                ecb.AddComponent(entity, new IsAttackEnableComponent()
                {
                    Value = isAttackEnableComponent
                });
            }
            
            foreach (var (syncGameObject, entity) in
                     SystemAPI.Query<SynchronizeGameObject>().WithAll<LocalTransform>().WithAny<PlayerTag, EnemyTag>().WithNone<AnimatorReference>().WithEntityAccess())
            {
                ecb.AddComponent(entity, new AnimatorReference()
                {
                    Value = syncGameObject.Value.GetComponent<Animator>()
                });
            }
            
            foreach (var (transform, syncGameObject) in
                     SystemAPI.Query<LocalTransform, SynchronizeGameObject>().WithNone<EnemyTag, DamageEntityTag>())
            {
                syncGameObject.Value.transform.position = transform.Position;
                syncGameObject.Value.transform.rotation = transform.Rotation;
            }
            
            foreach (var (transform, syncGameObject) in
                     SystemAPI.Query<LocalTransform, SynchronizeGameObject>().WithAll<DamageEntityTag>().WithNone<EnemyTag>())
            {
                syncGameObject.Value.transform.position = transform.Position;
            }
            
            foreach (var (transform, syncGameObject) in
                     SystemAPI.Query<RefRW<LocalTransform>, SynchronizeGameObject>().WithAll<EnemyTag>())
            {
                transform.ValueRW.Position = syncGameObject.Value.transform.position;
                transform.ValueRW.Rotation = syncGameObject.Value.transform.rotation;
            }
            
            foreach (var (isAttackEnableComponent, entity) in
                     SystemAPI.Query<IsAttackEnableComponent>().WithAll<LocalTransform, SynchronizeGameObject>().WithNone<PlayerTag>().WithEntityAccess())
            {
                ecb.SetComponentEnabled<AttackAllowedTag>(entity, isAttackEnableComponent.Value.Value);
            }

            foreach (var (syncGameObject, entity) in
                     SystemAPI.Query<SynchronizeGameObject>().WithNone<SynchronizeGameObjectPrefab, LocalTransform>()
                         .WithEntityAccess())
            {
                Object.Destroy(syncGameObject.Value);
                ecb.RemoveComponent<SynchronizeGameObject>(entity);
            }

            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }
    }
}