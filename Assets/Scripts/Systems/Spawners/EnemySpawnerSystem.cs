using System.Runtime.InteropServices;
using Components.ComponentsData.Spawners;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;

namespace Systems.Spawners
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(TransformSystemGroup))]
    [BurstCompile]
    [StructLayout(LayoutKind.Auto)]
    public partial struct EnemySpawnerSystem : ISystem, ISystemStartStop
    {
        private Entity _enemies;
     
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<EnemiesCount>();
        }
        
        [BurstCompile]
        public void OnStartRunning(ref SystemState state)
        {
            _enemies = SystemAPI.GetSingletonEntity<EnemiesCount>();
        }
        
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecb = new EntityCommandBuffer(Allocator.Temp);
            
            foreach (var (entityPrefab, transform) in
                     SystemAPI.Query<SpawnerEntityPrefab, LocalTransform>().WithAll<EnemySpawnerTag>())
            {
                if (SystemAPI.GetComponent<EnemiesCount>(_enemies).Value > 0) continue;
                
                var newEntity = ecb.Instantiate(entityPrefab.Value);

                var entityTransform = LocalTransform.FromPositionRotationScale
                    (transform.Position, transform.Rotation, 1.0f);

                ecb.SetComponent(newEntity, entityTransform);
            }
            
            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }

        public void OnStopRunning(ref SystemState state)
        {
            
        }
    }
}