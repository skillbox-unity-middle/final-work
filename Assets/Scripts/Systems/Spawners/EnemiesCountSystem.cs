using System.Runtime.InteropServices;
using Components.ComponentsData.Enemies;
using Components.ComponentsData.Spawners;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;

namespace Systems.Spawners
{
    [BurstCompile]
    [UpdateBefore(typeof(EnemySpawnerSystem))]
    [StructLayout(LayoutKind.Auto)]
    public partial struct EnemiesCountSystem : ISystem
    {
        private Entity _entity;
        
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            _entity = state.EntityManager.CreateEntity();
            state.EntityManager.AddComponent<EnemiesCount>(_entity);
        }
        
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecb = new EntityCommandBuffer(Allocator.Temp);

            var count = 0;
            
            foreach (var unused in SystemAPI.Query<EnemyTag>())
            {
                count++;
            }
            
            ecb.SetComponent(_entity, new EnemiesCount()
            {
                Value = count
            });
            
            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }
    }
}