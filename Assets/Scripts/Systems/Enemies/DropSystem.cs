using System.Runtime.InteropServices;
using Components.ComponentsData;
using Components.ComponentsData.Buffs;
using Components.ComponentsData.Enemies;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Systems.Enemies
{
    [BurstCompile]
    [UpdateInGroup(typeof(SimulationSystemGroup), OrderLast = true)]
    [UpdateBefore(typeof(DestroyEntitySystem))]
    [UpdateAfter(typeof(EndSimulationEntityCommandBufferSystem))]
    public partial struct DropSystem : ISystem
    {
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<BeginSimulationEntityCommandBufferSystem.Singleton>();
            state.RequireForUpdate<BuffSpawner>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecb = SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>()
                .CreateCommandBuffer(state.WorldUnmanaged);
            new DropJob()
            {
                ECB = ecb,
                BuffSpawner = SystemAPI.GetSingleton<BuffSpawner>()
            }.Schedule();
        }

        [BurstCompile]
        [WithAll(typeof(EnemyTag), typeof(DestroyTag))]
        [StructLayout(LayoutKind.Auto)]
        public partial struct DropJob : IJobEntity
        {
            public EntityCommandBuffer ECB;
            public BuffSpawner BuffSpawner;

            private const float OffsetY = 0.5f;
            
            [BurstCompile]
            private void Execute(ref RandomGenerator randomGenerator, ref LocalTransform localTransform)
            {
                var rand = randomGenerator.Value.NextFloat();

                var randomDrop = randomGenerator.Value.NextInt(0, 5);
                
                if (!(rand <= BuffSpawner.ChanceToDrop)) return;

                var newPosition = new float3(localTransform.Position.x, localTransform.Position.y + OffsetY,
                    localTransform.Position.z);
                var newRotation = quaternion.identity;
                var newTransform = LocalTransform.FromPositionRotationScale
                    (newPosition, newRotation, localTransform.Scale);
                switch (randomDrop)
                {
                    case 0:
                        var buffRateOfFireEntity = ECB.Instantiate(BuffSpawner.RateOfFireBuffEntityPrefab);
                        ECB.SetComponent(buffRateOfFireEntity, newTransform);
                        break;
                    case 1:
                        var buffMoveSpeedEntity = ECB.Instantiate(BuffSpawner.MoveSpeedBuffEntityPrefab);
                        ECB.SetComponent(buffMoveSpeedEntity, newTransform);
                        break;
                    case 2:
                        var buffHealthPointEntity = ECB.Instantiate(BuffSpawner.HealthPointBuffEntityPrefab);
                        ECB.SetComponent(buffHealthPointEntity, newTransform);
                        break;
                    case 3:
                        var buffDefenseEntity = ECB.Instantiate(BuffSpawner.DefenseBuffEntityPrefab);
                        ECB.SetComponent(buffDefenseEntity, newTransform);
                        break;
                    case 4:
                        var buffDamageEntity = ECB.Instantiate(BuffSpawner.DamageBuffEntityPrefab);
                        ECB.SetComponent(buffDamageEntity, newTransform);
                        break;
                }
            }
        }
    }
}