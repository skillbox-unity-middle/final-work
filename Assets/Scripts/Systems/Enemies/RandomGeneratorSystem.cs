using System.Runtime.InteropServices;
using Components.ComponentsData.Enemies;
using Unity.Burst;
using Unity.Entities;
using Random = Unity.Mathematics.Random;

namespace Systems.Enemies
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(DropSystem))]
    public partial struct RandomGeneratorSystem : ISystem
    {
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<BeginSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecb = SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>()
                .CreateCommandBuffer(state.WorldUnmanaged);
            
            new RandomGeneratorJob
            {
                ECB = ecb,
                ElapsedTime = SystemAPI.Time.ElapsedTime + 1
            }.Schedule();
        }
        
        [WithAll(typeof(EnemyTag))]
        [WithNone(typeof(RandomGenerator))]
        [StructLayout(LayoutKind.Auto)]
        public partial struct RandomGeneratorJob : IJobEntity
        {
            public EntityCommandBuffer ECB;
            public double ElapsedTime;
            
            private void Execute(Entity entity, [EntityIndexInQuery] int index)
            {
                var seconds = System.DateTime.Now.TimeOfDay.TotalSeconds;
                const int multiply = 100;
                var randomGenerator = new Random((uint)(ElapsedTime * (index + 1u) * seconds * multiply));
                
                ECB.AddComponent(entity, new RandomGenerator()
                {
                    Value = randomGenerator
                });
            }
        }
    }
}