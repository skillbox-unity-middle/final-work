using Components.ComponentsData;
using Components.ComponentsData.Attacks;
using Components.ComponentsData.Enemies;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Systems
{
    [UpdateInGroup(typeof(SimulationSystemGroup), OrderLast = true)]
    [UpdateBefore(typeof(DestroyEntitySystem))]
    [UpdateAfter(typeof(EndSimulationEntityCommandBufferSystem))]
    public partial struct FXSystem : ISystem
    {
        public void OnUpdate(ref SystemState state)
        {
            var ecb = new EntityCommandBuffer(Allocator.Temp);
            
            foreach (var (destroyFXPrefab, transform) in SystemAPI.Query<DestroyFXPrefab, LocalTransform>().WithAll<DestroyTag>())
            {
                var newPosition = new float3(transform.Position.x, transform.Position.y + 0.5f, transform.Position.z);
                Object.Instantiate(destroyFXPrefab.Value, newPosition, transform.Rotation);
            }
            
            foreach (var (spawnFXPrefab, transform,entity) in SystemAPI.Query<SpawnFXPrefab, LocalTransform>().WithAll<EnemyTag>().WithEntityAccess())
            {
                var newPosition = new float3(transform.Position.x, transform.Position.y + 0.5f, transform.Position.z);
                Object.Instantiate(spawnFXPrefab.Value, newPosition, transform.Rotation);
                ecb.RemoveComponent<SpawnFXPrefab>(entity);
            }
            
            foreach (var (hitVFX, transform) in SystemAPI.Query<HitVFX, LocalTransform>().WithAll<DestroyTag>())
            {
                Object.Instantiate(hitVFX.Value, transform.Position, transform.Rotation);
            }

            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }
    }
}