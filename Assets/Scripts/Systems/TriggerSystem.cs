using System.Runtime.InteropServices;
using Components.ComponentsData;
using Unity.Burst;
using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Systems;

namespace Systems
{
    [UpdateInGroup(typeof(PhysicsSystemGroup))]
    [UpdateAfter(typeof(PhysicsSimulationGroup))]
    [UpdateBefore(typeof(ExportPhysicsWorld))]
    [StructLayout(LayoutKind.Auto)]
    public partial struct TriggerSystem : ISystem
    {
        private ComponentLookup<TriggerWithTarget> _triggers;
        
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<EndSimulationEntityCommandBufferSystem.Singleton>();
            state.RequireForUpdate<SimulationSingleton>();

            _triggers = SystemAPI.GetComponentLookup<TriggerWithTarget>();
        }

        [BurstCompile]
        private struct TriggerEventJob : ITriggerEventsJob
        {
            public ComponentLookup<TriggerWithTarget> Triggers;
            public EntityCommandBuffer ECB;
            
            public void Execute(TriggerEvent triggerEvent)
            {
                Entity trigger;
                Entity target;
                
                if (Triggers.HasComponent(triggerEvent.EntityA))
                {
                    trigger = triggerEvent.EntityA;
                    target = triggerEvent.EntityB;
                }
                else
                {
                    if (Triggers.HasComponent(triggerEvent.EntityB))
                    {
                        trigger = triggerEvent.EntityB;
                        target = triggerEvent.EntityA;
                    }
                    else
                    {
                        return;
                    }
                }
                
                ECB.SetComponent(trigger, new TriggerWithTarget()
                {
                    Target = target
                });
                ECB.SetComponentEnabled<TriggerWithTarget>(trigger, true);
            }
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            _triggers.Update(ref state);
            var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            
            state.Dependency = new TriggerEventJob
            {
                Triggers = _triggers,
                ECB = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged)
            }.Schedule(SystemAPI.GetSingleton<SimulationSingleton>(), state.Dependency);
        }
    }
}