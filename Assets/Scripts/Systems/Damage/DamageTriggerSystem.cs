using System.Runtime.InteropServices;
using Components.ComponentsData;
using Components.ComponentsData.Damage;
using Components.ComponentsData.Player;
using Unity.Burst;
using Unity.Entities;

namespace Systems.Damage
{
    [BurstCompile]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(DestroyEntitySystem))]
    [StructLayout(LayoutKind.Auto)]
    public partial struct DamageTriggerSystem : ISystem
    {

        private ComponentLookup<DefenseComponent> _defenses;
        
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<EndSimulationEntityCommandBufferSystem.Singleton>();
            _defenses = SystemAPI.GetComponentLookup<DefenseComponent>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            _defenses.Update(ref state);
            var ecb = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>()
                .CreateCommandBuffer(state.WorldUnmanaged);
            
            new DamageTriggerJob
            {
                ECB = ecb,
                Defenses = _defenses
            }.Schedule();
        }

        [BurstCompile]
        [StructLayout(LayoutKind.Auto)]
        public partial struct DamageTriggerJob : IJobEntity
        {
            public EntityCommandBuffer ECB;
            public ComponentLookup<DefenseComponent> Defenses;
            
            [BurstCompile]
            private void Execute(ref DynamicBuffer<DamageTaker> damageBuffer, ref HealthPoint healthPoint, Entity entity)
            {
                ECB.SetComponentEnabled<DamageTaker>(entity, false);
                if (damageBuffer.IsEmpty) return;
                
                var totalDamage = 0;
                foreach (var damage in damageBuffer)
                {
                    totalDamage += damage.Value;
                }

                if (Defenses.HasComponent(entity))
                {
                    totalDamage = (int)(totalDamage * Defenses.GetRefRO(entity).ValueRO.Value);
                }
                
                healthPoint.CurrentHp -= totalDamage;
                damageBuffer.Clear();

                if (healthPoint.CurrentHp <= 0)
                {
                    ECB.AddComponent<DestroyTag>(entity);
                }
            }
        }
    }
}